import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UsersSchema extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('remember_me_token').nullable()
      table.timestamps(true,true)
      table.string('username', 100).notNullable().unique()
      table.string('email', 35).notNullable().unique()
      table.string('password', 100).notNullable()
      table.string('phone', 15).notNullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
