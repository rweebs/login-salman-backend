import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {schema,rules} from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'

export default class AuthController {
    public async register({request,response}:HttpContextContract){
    const validator = {
        schema: schema.create({
            email:schema.string({},[
                rules.required(),
                rules.email(),
                rules.unique({table: 'users', column: 'email'})
            ]),
            password:schema.string({},[
                rules.minLength(6),
                rules.required()

            ]),
            username:schema.string({},[
                rules.required(),
                rules.unique({table: 'users', column: 'username'})
            ]),
            phone:schema.string({},[
                rules.required()
                ])

            

        }),
        message:{
            'email.unique':'The email has already been taken.',
            'username.unique':'Username has already been taken.',
            'password.minLength':"The password must be at least 6 characters."
        }
    }
    const validated = await request.validate(validator)
    const newUser = new User();
    newUser.email=validated.email
    newUser.password=validated.password
    newUser.username=validated.username
    newUser.phone=validated.phone
    await newUser.save()
    return response.status(200).json({
        success: true,
        data:newUser
    });
    }

    public async login({request,auth,response}:HttpContextContract){
        const validator = {
            schema: schema.create({
                username:schema.string({},[
                    rules.required()
                   
                ]),
                password:schema.string({},[
                    rules.minLength(6),
    
                ]),
    
            }),
            messages:{
                'password.minLength':"The password must be at least 6 characters."
            }
        }
        const validated = await request.validate(validator)
        
        try {
            const token = await auth.use('api').attempt(validated.username, validated.password);
            return response.status(200).json({
                success: true,
                token: token.token
            });
        } catch(err) {
            return response.status(401).json({
                success: false,
                message: "Invalid username or password"
            })
        }

        }
    }